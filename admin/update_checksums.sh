#!/bin/bash
# notebooks can check self if they have been modified by
# an md5 reference checksum. This script updates the 
# checksum in the script that does the checking

for notebook in $(find .. -name "*ipynb" | grep -v checkpoints); do
  dirname=$(dirname $notebook)/src/.check
  basename=$(basename $notebook)
  [[ -d $(dirname $notebook)/src ]] || mkdir $(dirname $notebook)/src
  [[ -d $dirname ]] || mkdir $dirname
	md5sum $notebook > $dirname/$basename.md5
  printf "  Recreating checksum for \n\t $notebook \n\t in $dirname \n\n" 
done
