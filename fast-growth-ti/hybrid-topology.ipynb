{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Generating a hybrid topology containing two ligands"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```\n",
    "Author        : Christian Blau \n",
    "Goal          : Learn how to obtain ligand parameters   \n",
    "Time          : 20 minutes \n",
    "Prerequisites : basic molecular dynamics simulation\n",
    "                solvation free energy\n",
    "Software      : GROMACS 2020.1\n",
    "                openbabel\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Setting up and parametrizing ethanol and 1-propanol for the simulation<a class=\"anchor\" id=\"free-md\"></a>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note *We now contiune with the pracitical molecular dynamics part of this tutorial. Here, we assume that you know the basics of molecular dynamics simulation - have a look at, e.g., the lysozyme simulation tutorial if you want to familiarise yourself with these first*   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will first calculate the relative free energy of solution for ethanol and propanol."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we start the actual work in the directory. All current work is done in the `data` directory - we change path once and for all following commands."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# note - executing multiple times will result in a harmless error message\n",
    "%cd data  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Extracting the ligand conformation from the structure file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we extract an ethanol structure from the input file from the [pdb database](www.rcsb.org), which we provide for this tutorial under `input/1oof.pdb`. Knowing that ethanol is called EOH, we extract that from the pdb file directly:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!grep EOH input/1oof.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As an effect of crystallisation, the `pdb` file contains two symmetrical units of LUSH; chain A and chain B. We are happy with the ethanol atoms in chain A, so we use a regular expression to filter these out and write to a pdb file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "grep -r \"^HETATM.*EOH A\" input/1oof.pdb | \\\n",
    "sed 's/HETATM/ATOM  /'> ethanol-wo-h.pdb\n",
    "cat ethanol-wo-h.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The atom order in this ethanol molecule is diffferent from 1-propanol in the other `.pdb` used here. To make our lifes easier, we make things consistent. Note that this step is not strictly necessary, but drastically simplifies sanity-checking."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!tail -1 ethanol-wo-h.pdb > ethanol-wo-h-sorted.pdb\n",
    "!head -2 ethanol-wo-h.pdb >> ethanol-wo-h-sorted.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This ethanol structure as added to the pdb lacks hydrogens - let's add them here:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note: this step requires `openbabel`, an open source chemistry library. Download and install following these [instructions](https://openbabel.org/docs/dev/Installation/install.html). If you cannot or don't want to install openbabel here, we provide all the openbabel output in the `reference/` folder**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "obabel ethanol-wo-h-sorted.pdb -h -O ethanol-h.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's repeat the same process for propanol."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "grep -r \"^HETATM.*POL A\" input/1oog.pdb | sed 's/HETATM/ATOM  /'> propanol-wo-h.pdb"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "obabel propanol-wo-h.pdb -h -O propanol-h.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Obtaining reference data when you don't have `openbabel`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this command we copy all the reference data that otherwise would be created with `openbabel` to the current folder. Adding hydrogens is a common task that other tools like `pymol` can provide as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp reference/ethanol-h.pdb .\n",
    "cp reference/propanol-h.pdb .\n",
    "cp reference/ethanol.svg ."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Incorporating the ligand parameters into the simulation setup - topology generation without `pdb2gmx`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most pipelines for vanilla molecular dynamics simulations start with `pdb2gmx` to generate a system toplogy from a force-field. The molecule that we want to simulate, ethanol, is not available in the force-field - neither in any other of the \"standard\" force-fields. When we have to incorporate additional parameters for ligands, this standard routine is not available.\n",
    "\n",
    "To run molecular dynamics simualtions with new ligand parameters we can choose two paths:\n",
    " 1. extend force-field parameters in the force-field folder\n",
    "     - copy gromacs/share/top/force-field-of-choise.ff\n",
    "     - add ligand to residue database\n",
    "     - add new atomtyeps\n",
    "     - add non-bonded and bonded parameters to force-field\n",
    "     - add virtual sites if needed\n",
    "     - plus:\n",
    "         - multiple use and required for virtual site simulations\n",
    "     - minus:\n",
    "         - lengthy and error-prone procedure\n",
    "         - not suitable for alchemical transitions like slow as well as fast-growth thermodynamic integration\n",
    "         \n",
    " 1. **add the parameters to the topology**\n",
    "     - the way we choose in this tutorial \n",
    "     - new simulation setups will require new topology modification"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a parametrization result, you will usually obtain an `.itp` file. In this tutorial, we provide the molecule parameters for ethanol in the data folder under `input/ethanol-parameters`. Observe that this contains a most complete description of the molecule."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat input/ethanol-parameters/ethanol.top"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat input/ethanol-parameters/ethanol.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Matching atom names and structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We make sure that the atom names in the structure match the ones given in the toplogy. Note, that we might get away without, but will see errors when processing the structure with `grompp`. Also, a mismatch in structure and topology information will be fatal for the simualtions, because atoms will experience enourmous forces when they are mixed up."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed 's/EOH/LIG/' ethanol-h.pdb |  # the provided topology uses the generic \"LIG\" \\\n",
    "sed 's/A 500/A   1/'     |        # This is not residue 500 anymore              \\\n",
    "grep ATOM                |        # we're only interesed in the atoms            \\\n",
    "sed 's/1  O   /1  OC2 /' | \\\n",
    "sed 's/2  C1  /2   C2 /' |        # rename the atoms   \\\n",
    "sed 's/3  C2  /3   C3 /' | \\\n",
    "sed 's/4  H   /5 HC21 /' | \\\n",
    "sed 's/5  H   /6 HC22 /' | \\\n",
    "sed 's/6  H   /7 HC31 /' | \\\n",
    "sed 's/7  H   /8 HC32 /' | \\\n",
    "sed 's/8  H   /9 HC33 /' | \\\n",
    "sed 's/9  H   /4  HO2 /' | \\\n",
    "sort -k2 -n > ethanol.pdb                     # write the output\n",
    "cat ethanol.pdb # let's check what we just wrote to ethanol.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To avoid overwriting the ethanol topology file as some gromacs tools like `gmx solvate` will do, we copy the topology into our current working directory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp input/ethanol-parameters/ethanol.itp ethanol.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*Note tools like acpype and STaGE will provide you with ligand structure files that have the correct naming.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input parmeters for 1-propanol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Similarly to ethanol, we also parameterised 1-propanol. Find the input parameters under `input/propanol-parameters`. We copy also these to our current working directory"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "cp input/propanol-parameters/propanol.itp propanol.itp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "!cat propanol.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's make the naming consistent again.."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed 's/POL/LIG/' propanol-h.pdb |  # the provided topology uses the generic \"LIG\" \\\n",
    "sed 's/A 500/A   1/'     |        # This is not residue 500 anymore              \\\n",
    "grep ATOM                |        # we're only interesed in the atoms            \\\n",
    "sed 's/ 1  O   / 1  OC2 /' |        # rename the atoms   \\\n",
    "sed 's/ 2  C1  / 2   C2 /' | \\\n",
    "sed 's/ 3  C2  / 3   C3 /' | \\\n",
    "sed 's/ 4  C3  / 4   C4 /' | \\\n",
    "sed 's/ 5  H   / 5  HO2 /' | \\\n",
    "sed 's/ 6  H   / 6 HC21 /' | \\\n",
    "sed 's/ 7  H   / 7 HC22 /' | \\\n",
    "sed 's/ 8  H   / 8 HC31 /' | \\\n",
    "sed 's/ 9  H   / 9 HC32 /' | \\\n",
    "sed 's/10  H   /10 HC41 /' | \\\n",
    "sed 's/11  H   /11 HC42 /' | \\\n",
    "sed 's/12  H   /12 HC43 /' > propanol.pdb\n",
    "cat propanol.pdb # let's check what we just wrote to ethanol.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Transition ethanol to 1-propanol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Morphing strategies "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we \"morph\" one molecule into another. For this, we have to decide what atoms to alter in an ethanol molecule to obtain a 1-butanol molecule. Here, we would like to distort the system as little as possible and are therefore looking for the largest overlap between molecules. For an ethanol - 1-propanol transition this is still feasible manually, the larger the compounds and the number of compounds, the harder to find the optimal morph."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See two different transition strategies below. One entails changing a hydrogen at the $^2\\mathrm{C}$ atom into a $-\\mathrm{CH}_3$ group, whereas the other approach prefers transitioning the hydroxyl-oxygen into a carbon and adding adding a new hydroxyl group. Note that transitioning atom types brings with it other changes to the topology, depending on the force-field. The amber force-field, e.g., distinguishes different types of hydrogen atoms, depending on whether they are part of a methyl group or an alkyl chain.\n",
    "\n",
    "We proceed with the more intuitive strategy of adding a methyl group while the `atoms_to_morph.py` script from the `pmx` software package that uses geometrical alignment and maximum common subgraph prefers the strategy below, "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "![trans](images/morph-ethanol-propanol.svg)\n",
    "Result of the `pmx` command\n",
    "```bash\n",
    "python atoms_to_morph.py \n",
    "    -i1 ethanol-h.pdb -i2 propanol-h.pdb [ -mcs || -alignment ] -H2H -timeout 180 -o input/propanol-ethanol-pairs.dat\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat input/propanol-ethanol-pairs.dat"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial, we provide all the data required so that you don't have to install [`pmx`](http://pmx.mpibpc.mpg.de/) and all steps may be performed fully manually."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Generating \"hybrid\" topology that describes ethanol and 1-propanol simultaneously"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we want to generate a \"hybrid\" topology that allows a simulation to transition from ethanol to 1-propanol. You can generate such a topology \"by hand\", though this is a tedious and error-prone process, even for such a simple change from ethanol to 1-propanol. Let's have a look at the `[ atoms ]` section in the \"hybrid\" toplogy that was generated from `pmx` using \n",
    "\n",
    "```\n",
    "python make_hybrid.py -l1 ethanol.pdb -l2 propanol.pdb -itp1 ethanol.itp -itp2 propanol.itp -pairs input/propanol-ethanol-pairs.dat\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "grep -A 14 \"atoms\" input/merged.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note the new fields `typeB`, `chargeB`, and `massB`. All these correspond to the *B*-state structure, in this case 1-propanol."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### \"Dummy\" atoms and other merged atom types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Note how a transition from ethanol to propanol entails three types of atoms:\n",
    "\n",
    " * atoms that do not change - no interpolation\n",
    " * atoms that change from one type to another - interpolate between two different atom types and their interactions\n",
    " * atoms that appear from nothing - interpolate between \"nothingness\" and an atom with it's interactions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "The appaerance out of \"nothingness\" is not well defined - consider ,e.g., where a new atom should start to appear? \n",
    "If we were to introduce new atoms at an arbitrary position in the system, we would expect very high energies and forces. To circumvent the issue, we introduce atoms in the ethanol system already at positions where we expect the new atoms to appear, so called \"dummy atoms\". They have only bonded interactions to ethanol, but no long-range interactions, so they have no direct interactions with the rest of the system. Note that the introduction of these dummy atoms does alter the internal energy of the system, due to their interactions through bonds and dihedral angles with the ethanol molecule. The assumption here is, that the contributions from \"dummy\" atoms are small and cancel out when we calculate relative binding free energies.\n",
    "\n",
    "**TODO** figure dummy atoms in thermodynamic cycle"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Have a look at at the \"dummy\" atom types for atoms 10-12, `DUM_hc`. They are placeholders for the atoms to appear in the propanol structure; they have charge 0.0 and no van-der-Waals interaction in any state, as is apparent from their van-der-Waals paramters that are given in the following file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "cat input/ffmerged.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "When starting a simulation with a ligand that contains \"dummy\" atoms, it is useful to place the dummy atoms right away in a reasnoable position close to the ligand and in line with their `bond`, `dihedral` and `angle` parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "There are three strategies for placing \"dummy\" atoms:\n",
    " * place the dummy atoms arbitrarily and run a energy minimization simulation where all non-dummy atoms are restraint to their positions \n",
    " * use geometric considerations for placing the dummy atoms\n",
    " * overlay 1-propanol to estimate the dummy atom postions\n",
    " \n",
    "Here we used the last one, the overlay strategy, to place the dummy atoms. You can perform this with the tool of your choice, or, if usign `pmx` the output of `make_hybrid.py`. Here, the structure is provided as input/mergedA.pdb - we will use this to start our simulations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "!cp input/mergedA.pdb ethanol-merged.pdb"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "heading_collapsed": true
   },
   "source": [
    "### Finalising the \"merged\" topology"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Additionally to the `atoms`, changes occur in `bonds`, `angles` and `dihedrals`. Observe, e.g., how bond parameters have an A and a B-state, where some parameters differ."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "grep -A 12 \"bonds\" input/merged.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "We now combine all the force-field information into one merged `itp` file that contains all force-field information that is necessary to run simulations with the new hybrid ethanol-propanol ligand that is fully ethanol in a state A and fully propanol in a state B. When comparing the `ethanol.itp` file with the information in the `merged.itp` and `ffmerged.itp` you will notice that during that step we lost the `atomtype` information. We add that item of information back in. `gmx grompp` will warn us later if we missed obvious things like that, however note that `gmx grompp` cannot know about dihedral and angle paramters, which you will have to trust or check manually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "# adding in the missign [ atomtypes  ]\n",
    "cp input/ffmerged.itp ethanol-propanol.itp\n",
    "grep -A 7 atomtypes ethanol.itp | tail -6 >> ethanol-propanol.itp\n",
    "cat input/merged.itp >> ethanol-propanol.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Note that the atom names for hydrogens atoms 10, 11 and 12 in the topology are five characters long - while the `pdb` format only supports four. Let's fix this to make sure that we don't have to quiet warnings in programms like `grompp`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "%%bash\n",
    "sed -i 's/DHC4/ HC4/g' ethanol-propanol.itp"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "!cat ethanol-propanol.itp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": true
   },
   "source": [
    "Let's make the .pdb files match the topology"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hidden": true
   },
   "outputs": [],
   "source": [
    "!sed 's/10 DHC4/10 HC41/g' input/mergedA.pdb | sed 's/11 DHC4/11 HC42/g' | sed 's/12 DHC4/12 HC43/g' > mergedA.pdb  \n",
    "!sed 's/10 DHC4/10 HC41/g' input/mergedB.pdb | sed 's/11 DHC4/11 HC42/g' | sed 's/12 DHC4/12 HC43/g' > mergedB.pdb  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Generating a topology file"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Since we cannot use `pdb2gmx` to provide us with a topology, we write one from scratch.\n",
    "\n",
    "We require:\n",
    " * the force-field\n",
    " * the water parameters\n",
    " * the ion parameters\n",
    " * the hybrid-topology that we just created\n",
    " * the compounds in the system - for now only ethanol, `gmx` tools will help us update that information"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash\n",
    "echo '#include \"amber99sb-ildn.ff/forcefield.itp\"\n",
    "#include \"ethanol-propanol.itp\"\n",
    "#include \"amber99sb-ildn.ff/tip3p.itp\"\n",
    "#include \"amber99sb-ildn.ff/ions.itp\"\n",
    "\n",
    "\n",
    "[ system ]\n",
    "ethanol\n",
    "\n",
    "[ molecules ]\n",
    "; Compound    nmols\n",
    "  ethanol     1' > topol.top"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!cat topol.top"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
